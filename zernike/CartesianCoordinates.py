import numpy as np
import matplotlib.pyplot as plt
from zernike import RZern

cart = RZern(6)
L, K = 200, 250
ddx = np.linspace(-1.0, 1.0, K)
ddy = np.linspace(-1.0, 1.0, L)
xv, yv = np.meshgrid(ddx, ddy)
cart.make_cart_grid(xv, yv, unit_circle=False)

c0 = np.random.normal(size=cart.nk)
Phi = cart.eval_grid(c0, matrix=True)
c = cart.fit_cart_grid(Phi)
c1 = c[0]
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# plt.imshow(Phi, origin='lower')
ax.plot_surface(xv, yv,Phi,cmap=plt.cm.spring)
# plt.axis('off')
# plt.subplot(1, 2, 2)
# plt.plot(range(1, cart.nk + 1), c0, marker='.')
# plt.plot(range(1, cart.nk + 1), c1, marker='.')

plt.show()
